package game;

/**
 * A ghost attempts to move in a random direction at each round. A ghost is
 * blocked by artifacts. If a ghost lands on the player's current position, it
 * kills the player and the game is over by victory of the ghost (as opposed to
 * game over by player's suicide if the player lands on the ghost's cell).
 * 
 * @author Virginie Galtier
 */
public class Ghost extends GameCharacter {

	/**
	 * true when the ghost lands of the player's cell
	 */
	protected boolean isVictorious = false;

	@Override
	public void move(Cell toCell) {
		// the ghost can only move to an empty cell or a cell occupied by the player
		if (toCell.getContent() == null || !(toCell.getContent() instanceof Artifact)) {
			// leaves the previous cell
			this.cell.setContent(null);
			// maybe kill the player
			if (toCell.getContent() instanceof Player) {
				isVictorious = true;
			}
			// settles down in the new cell
			this.setCell(toCell);
		}
	}

	@Override
	public void play() {
		int direction = (int) (4 * Math.random());
		switch (direction) {
		case 0:
			System.out.println("Ghost decides to move North.");
			move(Board.NORTH);
			break;
		case 1:
			System.out.println("Ghost decides to move East.");
			move(Board.EAST);
			break;
		case 2:
			System.out.println("Ghost decides to move South.");
			move(Board.SOUTH);
			break;
		case 3:
			System.out.println("Ghost decides to move West.");
			move(Board.WEST);
			break;
		}
	}

	/**
	 * Tests if the ghost killed the player.
	 * @return true if the ghost lands of the player's cell
	 */
	public boolean isVictorious() {
		return isVictorious;
	}

	@Override
	public String getSymbol() {
		if (isVictorious) {
			return " V ";
		} else {
			return " G ";
		}
	}
}
