package game;

/**
 * A small board game.
 * 
 * @author Virginie Galtier
 */
import java.util.Scanner;

public class Game {

	/**
	 * available to components that needs to read commands from the keyboard
	 */
	public static Scanner scanner;

	public static void main(String[] args) {
		scanner = new Scanner(System.in);

		// creates the empty board
		Board board = new Board(4, 5);

		// creates the characters and sets them on the board
		Player player = new Player();
		board.add(0, 0, player);

		Ghost ghost = new Ghost();
		board.add(0, 3, ghost);

		// creates the artifacts and sets them on the board
		Drink drink1 = new Drink(5);
		board.add(3, 1, drink1);

		Drink drink2 = new Drink(4);
		board.add(1, 2, drink2);

		Obstacle obstacle1 = new Obstacle(3);
		board.add(2, 3, obstacle1);

		Obstacle obstacle2 = new Obstacle(5);
		board.add(2, 4, obstacle2);

		Obstacle obstacle3 = new Obstacle(3);
		board.add(3, 3, obstacle3);

		Obstacle obstacle4 = new Obstacle(2);
		board.add(3, 0, obstacle4);

		// shows initial configuration
		board.display();

		while (true) {
			player.play();

			if (player.wantsQuit()) {
				System.out.println("bye!");
				break;
			}
			if (player.isDead()) {
				board.display();
				System.out.println("game over...");
				break;
			}
			if (board.getExitCell().getContent() != null && board.getExitCell().getContent() instanceof Player) {
				board.display();
				System.out.println("You've reached the exit, congratulations!");
				break;
			}

			board.display();

			ghost.play();

			board.display();

			if (ghost.isVictorious()) {
				System.out.println("game over...");
				break;
			}
		}
		scanner.close();
	}
}
