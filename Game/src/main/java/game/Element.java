package game;

/**
 * An animate character or inanimate object that can be contained in a cell of
 * the game board.
 * 
 * @author Virginie Galtier
 */
public abstract class Element {

	/**
	 * The cell that contains the element.
	 */
	protected Cell cell;

	/**
	 * Sets or updates the position of the element. The board is updated
	 * accordingly.
	 * 
	 * @param cell initial or new position of the element
	 */
	public void setCell(Cell cell) {
		// set the position of the element
		this.cell = cell;
		// update the content of the board
		cell.setContent(this);
	}

	/**
	 * Gets a textual representation of the element.
	 * 
	 * @return a 3-characters long string representing the element
	 */
	public abstract String getSymbol();
}
