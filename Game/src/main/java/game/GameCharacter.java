package game;

/**
 * A character that decides of a new position at each round.
 * 
 * @author Virginie Galtier
 */
public abstract class GameCharacter extends Element {

	/**
	 * Decides of a new position and attempts to move there.
	 */
	public abstract void play();

	/**
	 * Attempts to move the character toward the given direction. (If the move is
	 * not possible because of an obstacle, the character stays at this current
	 * location and must wait for the next round to make a new attempt.)
	 * 
	 * @param direction ({@link game.Board#SOUTH SOUTH}, {@link game.Board#WEST
	 *                  WEST}, {@link game.Board#NORTH NORTH},
	 *                  {@link game.Board#EAST EAST})
	 */
	protected void move(int direction) {
		// queries the board to get the destination cell from the current position with
		// the given direction
		Cell toCell = cell.getBoard().findNextCell(cell, direction);
		// attempts to move from the current position to the new cell
		move(toCell);
	}

	/**
	 * Attempts to move from the current position to the new cell.
	 * 
	 * @param toCell destination cell
	 */
	protected abstract void move(Cell toCell);
}
