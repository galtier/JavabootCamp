package game;

/**
 * An inanimate object that can be dropped in a cell. The ghost cannot move to a
 * cell that contains an Artifact.
 * 
 * @author Virginie Galtier
 */
public abstract class Artifact extends Element {

}
