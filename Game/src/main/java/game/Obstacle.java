package game;

/**
 * An obstacle weights an amount of kilograms. If the level of energy of the
 * player is bigger than the weight of the obstacle, when the player moves to
 * the cell that contains the obstacle, the obstacle is moved from the cell to
 * the player's bag and the player's power is decreased by the weight of the
 * obstacle. An obstacle blocks the path of a player with insufficient power. An
 * obstacle can be moved from the player's bag to a cell when the player leaves
 * the cell.
 * 
 * @author Virginie Galtier
 */
public class Obstacle extends Artifact {

	/**
	 * weight of the obstacle
	 */
	protected int weight;

	/**
	 * Creates a new obstacle.
	 * 
	 * @param weight weight of the obstacle (required level of energy to move it)
	 */
	public Obstacle(int weight) {
		super();
		this.weight = weight;
	}

	/**
	 * Queries the weight of the obstacle.
	 * 
	 * @return weight of the obstacle
	 */
	public int getWeight() {
		return weight;
	}

	@Override
	public String getSymbol() {
		return " O" + weight;
	}
}
