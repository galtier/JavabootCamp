package game;

/**
 * A drink provides energy to the player. Once drunk, the drink disappears.
 * 
 * @author Virginie Galtier
 */
public class Drink extends Artifact {

	/**
	 * amount of energy provided by the drink
	 */
	protected int power;

	/**
	 * Creates a new Drink.
	 * 
	 * @param power amount of energy provided by the drink
	 */
	public Drink(int power) {
		super();
		this.power = power;
	}

	/**
	 * Queries the power of the drink.
	 * 
	 * @return amount of energy provided by the drink
	 */
	public int getPower() {
		return power;
	}

	@Override
	public String getSymbol() {
		return " D" + power;
	}
}
