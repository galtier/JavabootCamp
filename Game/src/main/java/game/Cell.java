package game;

/**
 * A cell of the board game.
 * 
 * @author Virginie Galtier
 */
public class Cell {

	/**
	 * coordinates of the cell: line gives the vertical position, starting at 0 at
	 * the top column gives the horizontal position, starting at 0 on the left
	 */
	private int line, column;
	/**
	 * the board the cell belongs to
	 */
	private Board board;
	/**
	 * cell content
	 */
	private Element content;

	/**
	 * Creates a new, empty cell.
	 * 
	 * @param line   cell line number (index starts at 0)
	 * @param column cell column number (index starts at 0)
	 * @param board  board game the cell belongs to
	 */
	public Cell(int line, int column, Board board) {
		this.line = line;
		this.column = column;
		this.board = board;
	}

	/**
	 * Gets a short textual representation of the content of the cell.
	 * 
	 * @return a 3-characters string to represent the cell content
	 */
	public String getSymbol() {
		if (content == null) {
			return "   ";
		} else {
			return content.getSymbol();
		}
	}

	/**
	 * Gets the vertical coordinate of the cell.
	 * 
	 * @return line number of the cell in the board game (starting at 0 at the top)
	 */
	public int getLine() {
		return line;
	}

	/**
	 * Gets the horizontal coordinate of the cell.
	 * 
	 * @return column number of the cell in board game (starting at 0 on the left)
	 */
	public int getColumn() {
		return column;
	}

	/**
	 * Gets the board the cell belongs to.
	 * 
	 * @return the board the cell belongs to
	 */
	public Board getBoard() {
		return board;
	}

	/**
	 * Gets the content of the cell.
	 * 
	 * @return the content of the cell
	 */
	public Element getContent() {
		return content;
	}

	/**
	 * Sets or updates the content of the cell.
	 * 
	 * @param content initial or new content of the cell
	 */
	public void setContent(Element content) {
		this.content = content;
	}

}
