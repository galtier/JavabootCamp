package game;

import java.util.ArrayList;
import java.util.List;

/**
 * A player controlled by the user.
 * 
 * @author Virginie Galtier
 */
public class Player extends GameCharacter {

	/**
	 * level of energy
	 */
	protected int power = 0;
	/**
	 * bag that contains the obstacles collected by the player
	 */
	protected List<Obstacle> bag;
	/**
	 * is the player dead (met the ghost...)
	 */
	protected boolean isDead = false;
	/**
	 * does the user want to quit the game
	 */
	protected boolean wantsQuit = false;

	/**
	 * Creates a new player.
	 */
	public Player() {
		bag = new ArrayList<Obstacle>();
	}

	@Override
	public void move(Cell toCell) {
		if (toCell.getContent() != null && toCell.getContent() instanceof Obstacle
				&& ((Obstacle) toCell.getContent()).getWeight() > power) {
			// the destination cell contains a too heavy obstacle
			System.out.println("This obstacle is too heavy for you...");
		} else {
			// the player can move to the destination cell

			// clears the current cell
			this.cell.setContent(null);

			if (toCell.getContent() != null && toCell.getContent() instanceof Ghost) {
				// destination cell contains the ghost: death by suicide...
				isDead = true;
			} else {
				// destination doesn't contain the ghost
				if (!this.bag.isEmpty()) {
					// before moving to destination cell, the player can leave an obstacle behind
					System.out.println(
							"press u to unload an obstacle before leaving your location, and k to keep your obstacles in you bag");

					String input = Game.scanner.nextLine();
					char choice = input.charAt(0);
					if (choice == 'u') {
						Obstacle obstacle = bag.remove(0);
						this.cell.setContent(obstacle);
						obstacle.setCell(cell);
					}
				}
				if (toCell.getContent() != null && toCell.getContent() instanceof Drink) {
					// destination contains a drink: increases energy
					this.power = ((Drink) toCell.getContent()).getPower();
					// clears the cell from the drink
					// toCell.setContent(null); // useless because the cell is soon occupied by the
					// player
				}
				if (toCell.getContent() != null && toCell.getContent() instanceof Obstacle) {
					// destination contains an obstacle that the player is able to load
					// loads obstacle inside the bag
					Obstacle obstacle = (Obstacle) toCell.getContent();
					this.bag.add(obstacle);
					// clears the cell from the obstacle
					// toCell.setContent(null); // useless because the cell is soon occupied by the
					// player
					// decreases energy
					power -= obstacle.getWeight();
				}
			}
			// settles down in the new cell
			setCell(toCell);
		}
	}

	@Override
	public void play() {
		String input;
		char command = ' ';
		System.out.println("your power = " + this.power);
		System.out.print("obstacles in your bag = ");
		for (Obstacle o : bag) {
			System.out.print(o.getSymbol());
		}
		System.out.println("\nz: go north, d: go east, s: go south, q: go west, e: end");
		input = Game.scanner.nextLine();
		command = input.charAt(0);

		switch (command) {
		case 'e':
			wantsQuit = true;
			break;
		case 'z':
			move(Board.NORTH);
			break;
		case 'd':
			move(Board.EAST);
			break;
		case 's':
			move(Board.SOUTH);
			break;
		case 'q':
			move(Board.WEST);
			break;
		}
	}

	/**
	 * Tests if the player commanded to exit the game.
	 * 
	 * @return true if the player issued the "end" command
	 */
	public boolean wantsQuit() {
		return wantsQuit;
	}

	/**
	 * Tests if the player landed on the ghost's cell.
	 * 
	 * @return true if the player's command lead him to the ghost's cell
	 */
	public boolean isDead() {
		return isDead;
	}

	@Override
	public String getSymbol() {
		if (isDead) {
			return " X ";
		} else {
			return " P ";
		}
	}
}
