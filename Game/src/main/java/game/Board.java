package game;
/**
 * The game board.
 * 
 * @author Virginie Galtier
 */
public class Board {

	/**
	 * direction: toward the bottom, or first line if the bottom is reached
	 */
	public static final int SOUTH = 0;
	/**
	 * direction: toward the left, or right-most column if the left-most is reached
	 */
	public static final int WEST = 1;
	/**
	 * direction: toward the top, or last line if the top is reached
	 */
	public static final int NORTH = 2;
	/**
	 * direction: toward the right, or left-most if the right-most is reached
	 */
	public static final int EAST = 3;
	/**
	 * array of the cells
	 */
	private Cell[][] board;
	/**
	 * dimensions of the board
	 */
	private int nbLines, nbColumns;
	/**
	 * cell chosen as target exit cell
	 */
	private Cell exitCell;

	/**
	 * Creates a new, empty board.
	 * @param nbLines height of the board (number of lines)
	 * @param nbColumns width of the board (number of columns)
	 */
	public Board(int nbLines, int nbColumns) {
		this.nbLines = nbLines;
		this.nbColumns = nbColumns;
		board = new Cell[nbLines][nbColumns];
		for (int line=0; line<nbLines; line++) {
			for (int column=0; column<nbColumns; column++) {
				board[line][column] = new Cell(line, column, this);
			}
		}
		exitCell =  board[nbLines-1][nbColumns-1];
	}
	/**
	 * Displays the board, with the content of the cells.
	 */
	public void display() {
		System.out.print(" ");
		for (int column=0; column<nbColumns; column++) {
			System.out.print("-----");
		}
		System.out.print("\n");

		for (int line=0; line<nbLines; line++) {
			System.out.print(" |");
			for (int column=0; column<nbColumns; column++) {
				System.out.print(" " + board[line][column].getSymbol() + "|");
			}
			System.out.print("\n ");
			for (int column=0; column<nbColumns; column++) {
				System.out.print("-----");
			}
			System.out.print("\n");
		}		
	}

	/**
	 * Adds an element into a cell of the board.
	 * @param line vertical position for the insert (index starts at 0 at the top)
	 * @param column horizontal position for the insert (index starts at 0 on the left)
	 * @param element element to insert on the board
	 */
	public void add(int line, int column, Element element) {
		// add the element to the board
		//board[line][column].setContent(element);		
		// inform the element of its location
		element.setCell(board[line][column]);
	}

	/**
	 * Gets the next cell in the given direction from the given cell.
	 * @param fromCell starting location
	 * @param direction moving direction
	 * @return the next cell in the given direction from the given cell
	 */
	public Cell findNextCell(Cell fromCell, int direction) {
		Cell toCell = null;
		switch (direction) {
		case SOUTH:
			toCell = board[(fromCell.getLine()+1)%nbLines][fromCell.getColumn()];	
			break;
		case NORTH:
			int line = fromCell.getLine()-1;
			if (line >= 0) {
				toCell = board[fromCell.getLine()-1][fromCell.getColumn()];
			} else {
				toCell = board[nbLines-1][fromCell.getColumn()];
			}
			break;
		case EAST:
			toCell = board[fromCell.getLine()][(fromCell.getColumn()+1)%nbColumns];
			break;
		case WEST:
			int colum = fromCell.getColumn()-1;
			if (colum >= 0) {
				toCell = board[fromCell.getLine()][fromCell.getColumn()-1];
			} else {
				toCell = board[fromCell.getLine()][nbColumns-1];
			}
			break;
		}
		return toCell;
	}

	/**
	 * Gets the cell chosen to be the exit cell
	 * @return the cell chosen to be the exit cell
	 */
	public Cell getExitCell() {
		return board[nbLines-1][nbColumns-1];
	}
}
