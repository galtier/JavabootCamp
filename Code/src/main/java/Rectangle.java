public class Rectangle {

	// attributes:
	double width;
	double length;
	static int nbRectangles = 0;

	// constructor:
	Rectangle(double w, double l) {
		width = w;
		length = l;
		nbRectangles++;
	}

	// methods that read attributes:
	double getArea() {
		return (length * width);
	}
	double getPerimeter() {
		return 2 * (length + width);
	}

	// method that changes attributes: 
	void resize(double coefficient) {
		length = length * coefficient;
		width = width * coefficient;
	}

	// displayer:
	void printDescription() {
		System.out.println("width = " + width + ", length = " + length);
	}
}

