/**
 * For now, keep the 'main' method minimalist 
 * and write all the code in the constructor
 * as in the following example.
 */
package bootcamp.part1._03_helloWorldAlt;

public class HelloWorld {

	public static void main(String[] args) {
		new HelloWorld();
	}
	
	public HelloWorld() {
		System.out.println("Hello world!");
	}
}
