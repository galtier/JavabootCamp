/**
 * Add a cast where needed.
 * Observe the display values.
 */
package bootcamp.part1._06_cast;

public class HelloWorld {

	public static void main(String[] args) {
		new HelloWorld();
	}
	
	public HelloWorld() {
		// int is double
		int i = 3;
		double d = i;
		System.out.println("i = " + i + "\t d = " + d);
		
		// double is not int
		double pi = 3.14;
		int n = pi;
		d = n;
		System.out.println("pi=" + pi + " n=" + n + " d=" + d);
	}
}
