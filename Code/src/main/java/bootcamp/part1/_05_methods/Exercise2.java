/**
 * Write a method atLeastOneFalse that takes 3 boolean values as parameters 
 * and returns true if at least one of them is false, and false otherwise.
 * (Use the Java Memento to learn about boolean operations syntax.)
 */
package bootcamp.part1._05_methods;

public class Exercise2 {

	public static void main(String[] args) {
		new Exercise2();
	}

	public Exercise2() {
		System.out.println(atLeastOneFalse(true, true, true)); // expected return: false
		System.out.println(atLeastOneFalse(true, true, false)); // expected return: true
		System.out.println(atLeastOneFalse(false, true, false)); // expected return: true
	}

}
