/**
 * Write a method displayProduct that takes 2 integer values (int) as parameters 
 * and displays their product using the following format:
 * 2 * 3 = 6 (first operand, space character, "*", space character, "=", space, result)
 */
package bootcamp.part1._05_methods;

public class WithParamNoReturn {

	public static void main(String[] args) {
		new WithParamNoReturn();
	}
	
	public WithParamNoReturn() {
		displayProduct(2, 3);
	}
}
