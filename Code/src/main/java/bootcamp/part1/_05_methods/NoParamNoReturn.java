/**
 * Write a hello method that doesn't take parameters and 
 * doesn't return a result but simply displays "hello".
 */
package bootcamp.part1._05_methods;

public class NoParamNoReturn {

	public static void main(String[] args) {
		new NoParamNoReturn();
	}
	
	public NoParamNoReturn() {
		hello();
	}
	
	// write the hello method here
}
