package bootcamp.part1._04_variables;

public class PrimitiveVarDemo {

	public static void main(String[] args) {
		new PrimitiveVarDemo();
	}
	
	public PrimitiveVarDemo() {
		int nb; // declare variable type and name
		nb = 3; // assign value ("affectation" en français)
		
		int n = 4; // declaration and value assignment in one line
		
		boolean isGood = false; // !!! "False" in Python, but "false" in Java
		
		char a = 'e';
		char c;
		c = a;
		System.out.println(c);
		
		// Which instructions are correct?
		// Uncomment each line, one at a time to find out.
		// c = 'a';
		// c = a;
		// c = '\n';
	    // c = null;
		// c = "a";
		// c = '';
		// c = b;
		
		// example of runtime error
		int q = 3/0;
	}
}
