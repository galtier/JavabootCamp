/**
 * Write a method that displays the content of an array.
 */
package bootcamp.part1._09_arrays;

import java.util.Arrays;

public class Exercise2 {

	public static void main(String[] args) {
		new Exercise2();
	}
	
	public Exercise2() {
		int[] a = {1, 2, 3, 4};   // shortcut to declaration + creation + assignment
		display(a);
	}
	
}
