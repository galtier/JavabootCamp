/**
 * Write an 'init' method that takes an integer value as parameter and returns an array of integers filled as below:
 * init(4):
 *     [0, 1, 2, 3]
 */
package bootcamp.part1._09_arrays;

public class Exercise1 {
	public static void main(String[] args) {
		new Exercise1();
	}
	
	public Exercise1() {
		int[] tab = init(4);
		System.out.println(tab.length); // expected: 4
		System.out.println(tab[0]); // expected: 0
		System.out.println(tab[3]); // expected 3
	}
}
