/**
 * Use artEoz to understand how Java arrays are represented in memory
 */
package bootcamp.part1._09_arrays;

import java.util.Arrays;

public class ArrayDemo {

	public static void main(String[] args) {
		new ArrayDemo();
	}
	
	public ArrayDemo() {
		int[] t;  		// declaration
		t = new int[2]; // creation
		t[0] = 33;      // first cell assignment
		System.out.println(t.length);  // 1 cell explicitly initialized but 2 existing cells
		System.out.println(t[0]);   // access to first cell
		//System.out.println(t[2]);   // index starts at 0, t[2] doesn't exist, triggers a runtime error
		
		System.out.println(t);  // for non primitive objects, the print method displays "type@hashcodeOfObject"
		System.out.println(java.util.Arrays.toString(t)); // equires to import the Arrays package
		
		int[] a = {1, 2, 3, 4};   // shortcut to declaration + creation + assignment
		int[] b = a;              // b "points" towards the same memory cell as a
		b[0] = 100;
		a = new int[] {10, 20};
		System.out.println(b[0]);
	}
}
