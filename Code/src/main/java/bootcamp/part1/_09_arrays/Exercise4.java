/**
 * Write a `separate` method that takes an array of integer values as parameter 
 * and reorganizes it so the even values are at the beginning of the array and the odd values at the end 
 * (difference from the previous exercise: the initial array is modified).
 */
package bootcamp.part1._09_arrays;

import java.util.Arrays;

public class Exercise4 {

	public static void main(String[] args) {
		new Exercise4();
	}

	public Exercise4() {
		int[] a = {1, 2, 3, 4};
		int[] b = {1, 2, 3, 4};
		separateBis(a);
		System.out.println(Arrays.toString(a));
		System.out.println(check(b, a));
	}

	
	private static boolean check(int[] tabA, int[] tabB) {
    	boolean ok = true;
    	// check even // odd    	
		boolean even = true;
		for (int i = 0; i < tabB.length; i++) {
			if (even && (tabB[i] % 2 != 0)) {
				even = false;
			}
			if (!even && tabB[i] % 2 == 0) {
				ok = false;
			}
		}
		// check contain the same numbers
		Arrays.sort(tabA);
    	Arrays.sort(tabB);
    	for (int i = 0; i < tabA.length; i++) {
    		if (tabA[i] != tabB[i]) {
    			ok = false;
    			break;
    		}
    	}
    	
		return ok;
	}

}
