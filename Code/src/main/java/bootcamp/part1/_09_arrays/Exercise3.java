/**
 * Write a separate method that takes an array of integer values as parameter 
 * and return a new array with all the even values are at the beginning of the array 
 * and the odd values at the end.
 */
package bootcamp.part1._09_arrays;

import java.util.Arrays;

public class Exercise3 {

	public static void main(String[] args) {
		new Exercise3();
	}

	public Exercise3() {
		int[] a = {1, 2, 3, 4};
		int[] b = separate(a);
		System.out.println(Arrays.toString(a)); // [1, 2, 3, 4]
		System.out.println(Arrays.toString(b)); // [1, 3, 4, 2] or [3, 1, 4, 2] or ...
		System.out.println(check(a, b)); // true
	}

	

    private static boolean check(int[] tabA, int[] tabB) {
    	boolean ok = true;
    	// check even // odd    	
		boolean even = true;
		for (int i = 0; i < tabB.length; i++) {
			if (even && (tabB[i] % 2 != 0)) {
				even = false;
			}
			if (!even && tabB[i] % 2 == 0) {
				ok = false;
			}
		}
		// check contain the same numbers
		Arrays.sort(tabA);
    	Arrays.sort(tabB);
    	for (int i = 0; i < tabA.length; i++) {
    		if (tabA[i] != tabB[i]) {
    			ok = false;
    			break;
    		}
    	}
    	
		return ok;
	}

}
