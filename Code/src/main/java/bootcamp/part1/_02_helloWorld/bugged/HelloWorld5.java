/**
 * Read this program and try to find the problem.
 * Attempt to execute the program and observe the error message.
 * Correct the problem so the program executes and displays "Hello world!".
 */
package bootcamp.part1._02_helloWorld.bugged;

public class HelloWorld5 {

	public static void main(String[] args) {
		System.out.display("Hello world!");
	}
}
