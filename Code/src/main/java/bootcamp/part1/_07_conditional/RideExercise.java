/**
 * The fare to a ride depends on the membership and age of the rider: 
 * non-members pay a price equal to their age, while members get 10% off. 
 * Write a method `double getFare(boolean isMember, int age)` to compute the fare.
 */
package bootcamp.part1._07_conditional;

public class RideExercise {

	public static void main(String[] args) {
		new RideExercise();
	}
	
	public RideExercise() {
		System.out.println(getFare(false, 9)); // expected result: 9
		System.out.println(getFare(true, 9));  // expected result: 8.1
		System.out.println(getFare(true, 5));  // expected result: 4.5
	}
}
