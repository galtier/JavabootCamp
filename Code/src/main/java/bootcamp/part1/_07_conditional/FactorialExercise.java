/**
 * Write a recursive method to compute the factorial of an integer given as parameter.
 */
package bootcamp.part1._07_conditional;

public class FactorialExercise {

	public static void main(String[] args) {
		new FactorialExercise();
	}

	public FactorialExercise() {
		System.out.println(factorial(0)); // expected result: 1
		System.out.println(factorial(1)); // expected result: 1
		System.out.println(factorial(5)); // expected result: 120
		System.out.println(factorial(12)); // expected result: 479,001,600
	}
	
	int factorial(int n) {
		return n * factorial(n-1);
	}

}
