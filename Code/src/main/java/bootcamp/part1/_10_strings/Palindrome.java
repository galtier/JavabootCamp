/**
 * Write a `isPalindrome` method that tests if the character string given as parameter is a palindrome. 
 * (Ignore white spaces, capitalization, and punctuation.)
 */
package bootcamp.part1._10_strings;

public class Palindrome {

	public static void main(String[] args) {
		new Palindrome();
	}

	public Palindrome() {
		System.out.println(isPalindrome("kayak")); //true
		System.out.println(isPalindrome("Was it a car or a cat I saw?")); // true
		System.out.println(isPalindrome("foo")); // false
	}
}
