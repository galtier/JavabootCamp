/**
 * Replace the "for" loop of this iterative method by a "while" loop.
 */
package bootcamp.part1._08_loops;

public class IterativeFactorialExercise {

	public static void main(String[] args) {
		new IterativeFactorialExercise();
	}
	
	public IterativeFactorialExercise() {
		System.out.println(factorial(0)); // expected result: 1
		System.out.println(factorial(1)); // expected result: 1
		System.out.println(factorial(5)); // expected result: 120
		System.out.println(factorial(12)); // expected result: 479,001,600
	}

	int factorial(int n) {
		int f = 1;
		for (int i=1; i <= n; i++) {
			f = f*i;
		}
		return f;
	}
}
