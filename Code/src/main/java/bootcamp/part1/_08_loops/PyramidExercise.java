/**
 * Using "for" loops, write a pyramid method that displays a pyramid of numbers whose height is given as a parameter:
 * pyramid(4) displays
 * 1
 * 21
 * 321
 * 4321
 */
package bootcamp.part1._08_loops;

public class PyramidExercise {

	public static void main(String[] args) {
		new PyramidExercise();
	}

	public PyramidExercise() {
		pyramid(4);
	}

}
