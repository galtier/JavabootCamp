package bootcamp.part3;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileWriteExamples {

	public static void main(String[] args) {

		// files are just another output stream:
		try {
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out));
			writer.write("Hello");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter("myFile.txt"));
			writer.write("Hello");
			writer.close();

			// append to an existing file:
			boolean append = true;
			writer = new BufferedWriter(new FileWriter("myFile.txt", append));
			writer.append("\nThis is the second line.");
			writer.newLine();
			writer.write("The end.");
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// to edit an existing file (rather than just appending content at the end):
		// use a RandomAccessFile

		// from Java 7: Files
		try {
			Path path = Paths.get("myOtherFile.txt");
			Files.write(path, "Hello Java".getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
