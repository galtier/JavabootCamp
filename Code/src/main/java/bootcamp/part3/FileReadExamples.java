package bootcamp.part3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class FileReadExamples {

	public static void main(String[] args) {

		// with File and Scanner
		try {
			Scanner scanner = new Scanner(new File("pom.xml"));
			scanner.useDelimiter("\n");
			while (scanner.hasNext()) {
				System.out.println(scanner.next());
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// with Java new IO
		Path path = Paths.get("pom.xml");
		List<String> fileContent;
		try {
			fileContent = Files.readAllLines(path);
			for (String line : fileContent) {
				System.out.println(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
