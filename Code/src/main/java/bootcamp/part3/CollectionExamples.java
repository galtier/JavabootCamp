package bootcamp.part3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class CollectionExamples {

	public static void main(String[] args) {
		// declaration using the generic interface, parameterized with String type:
		List<String> names;
		// creation using the ArrayList implementation:
		names = new ArrayList<String>();
		// adds elements:
		names.add("Alice"); // at the end
		names.add("Charly");
		names.add(1, "Bob"); // at a specific index
		// iterates with index and get():
		for (int i = 0; i < names.size(); i++) {
			System.out.println(names.get(i));
		}
		// List are Iterable:
		Iterator<String> iterator = names.iterator();
		// performs an action for each element:
		iterator.forEachRemaining(new Consumer<String>() {
			public void accept(String s) {
				System.out.println(s);
			}
		});
		iterator = names.iterator(); // resets the iterator at the beginning
		// iterates with hasNext and next:
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
}
