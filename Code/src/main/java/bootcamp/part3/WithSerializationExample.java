package bootcamp.part3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class WithSerializationExample {

	public static void main(String[] args) {
		List<Person> members = new ArrayList<Person>();
		members.add(new Person("Alice", 20));
		members.add(new Person("Bob", 22));
		members.add(new Person("Charly", 20));

		// save
		try {
			OutputStream outputStream = new FileOutputStream("members.txt");
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
			objectOutputStream.writeObject(members);
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// restore
		List<Person> savedMembers = new ArrayList<Person>();
		try {
			InputStream inputStream = new FileInputStream("members.txt");
			ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
			savedMembers = (List<Person>) objectInputStream.readObject();
			objectInputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		for (Person p : savedMembers) {
			System.out.print(p);
		}
	}

}
