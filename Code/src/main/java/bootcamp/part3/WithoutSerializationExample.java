package bootcamp.part3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class WithoutSerializationExample {

	public static void main(String[] args) {
		List<Person> members = new ArrayList<Person>();
		members.add(new Person("Alice", 20));
		members.add(new Person("Bob", 22));
		members.add(new Person("Charly", 20));

		// save
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter("members.txt"));
			for (Person member : members) {
				String s = member.getName() + "::" + member.getAge() + "##";
				writer.write(s);
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// restore
		List<Person> savedMembers = new ArrayList<Person>();
		try {
			Scanner scanner = new Scanner(new File("members.txt"));
			scanner.useDelimiter("##");
			while (scanner.hasNext()) {
				String line = scanner.next();
				String[] elements = line.split("::");
				Person p = new Person(elements[0], Integer.parseInt(elements[1]));
				savedMembers.add(p);
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		for (Person p : savedMembers) {
			System.out.print(p);
		}
	}

}
