package bootcamp.part3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

public class SystemInExamples {

	public static void main(String[] args) {

		System.out.print("enter your words: ");
		InputStream inputStream = System.in;
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		String line;
		try {
			line = bufferedReader.readLine();
			System.out.println("input = " + line);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Scanner scanner;
		scanner = new Scanner(inputStream);

		int n = 0;
		do {
			System.out.print("give a number: ");
			n = scanner.nextInt();
		} while (n < 10);

		String command = "";
		do {
			System.out.print("give your command: ");
			command = scanner.nextLine();
		} while (!command.equals("quit"));

		scanner.close();
	}
}
