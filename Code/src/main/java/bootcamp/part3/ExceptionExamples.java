package bootcamp.part3;

public class ExceptionExamples {

	public static void main(String[] args) {
		int[] tab1 = { 10, 20, 30, 40 };
		int[] tab2 = null;
		int index = (int) (7 * Math.random());
		System.out.println("index = " + index);
		try {
			System.out.println("tab1[" + index + "] = " + tab1[index]);
			System.out.println("tab2[0] = " + tab2[0]);
		} catch (ArrayIndexOutOfBoundsException oob) {
			System.out.println("oups! The index was not in the [0-3] range..." + oob.getMessage());
		} catch (NullPointerException np) {
			System.out.println("did you forget to create something new?");
		} finally {
			System.out.println("end of the delicate section");
		}
		System.out.println("good bye");
	}
}
