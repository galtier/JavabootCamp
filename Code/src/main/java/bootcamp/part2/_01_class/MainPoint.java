package bootcamp.part2._01_class;

public class MainPoint {

	public static void main(String[] args) {
		Point p1;
		p1 = new Point("#1");
		p1.printPosition(); // expected: #1: < -8 ; -2 > (with x and y random in [-10 ; 10[)
		
		Point p2 = new Point(0, 0, "origin");
		p2.printPosition(); // expected: origin: < 0 ; 0 >
		
	}

}
