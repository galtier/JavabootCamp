/**
 * Write a class Point. 
 * A point is characterized by its coordinates x and y (integer values) 
 * and its label (a character such as x, . or o for instance, to be used when the point is displayed). 
 * 
 * To create a point, the user must provide a label. 
 * If the user provides coordinates, they are passed as 2 integer values before the label. 
 * If the user doesn't provide coordinates, the point is given a random position with -10 <= x < 10 and -10 <= y < 10 
 * (Hint: use Math.random() to get a number in [0;1[). 
 * 
 * Add a method printPosition that displays the label of the point followed by its position with the following format: x: < -3 ; 4 >
 * 
 */
package bootcamp.part2._01_class;

class Point {
	int x;
    int y;
    String label;
    
    Point(String lab) {
        label = lab;
        x = (int)(Math.random()*20 - 10);
        y = (int)(Math.random()*20 - 10);
    }
    
    Point(int x_coordinate, int y_coordinate, String lab) {
        x = x_coordinate;
        y = y_coordinate;
        label = lab;
    }
    
    void printPosition() {
        System.out.println(label + ": < " + x + " ; " + y + " >");
    }
}
