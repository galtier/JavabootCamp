package bootcamp.part2._05_inheritance;

public class MainLib {

	public static void main(String[] args) {
		Document[] library = new Document[3];
		library[0] = new Book("Data Mining et statistique décisionnelle", "Editions Technip", "S. Tuffery", 379);
		library[1] = new Book("Algorithmique et Programmation en Java", "Dunod",  "V. Granet", 337);
		library[2] = new Journal("Computing in Science & Engineering - Volume: 20, Issue: 5 ,Oct. 2018", "IEEE", 18);
		for (int i = 0; i < library.length; i++) {
			library[i].getInfo();
			System.out.print("\n");
		}
	}
}
