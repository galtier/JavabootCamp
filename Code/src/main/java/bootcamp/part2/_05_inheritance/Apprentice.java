package bootcamp.part2._05_inheritance;

public class Apprentice extends Student {
	protected String company;
	Apprentice(String name, int gradYear, String company) {
		super(name, gradYear);
		this.company = company;
	}
	void enter() {
	    isIn = true;
	    System.out.print("Apprentice " + name + " stepped in.");
    }	
}