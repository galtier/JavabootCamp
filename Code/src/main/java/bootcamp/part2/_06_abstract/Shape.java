package bootcamp.part2._06_abstract;

abstract class Shape {
	   protected abstract double getArea();
	   protected abstract double getPerimeter();
	}