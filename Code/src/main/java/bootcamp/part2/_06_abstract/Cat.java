package bootcamp.part2._06_abstract;

public class Cat extends Animal {

	protected Cat(int age) {
		super(age);
	}

	@Override
	String getSound() {
		return "meow!";
	}

}
