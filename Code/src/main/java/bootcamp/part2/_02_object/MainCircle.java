package bootcamp.part2._02_object;

public class MainCircle {
	
	public static void main(String[] args) {
		new MainCircle();
	}
	
	MainCircle() {
		Point p = new Point("O, 0.0, 0.0");
		Circle c1 = new Circle(p, 10);
		Circle c2 = new Circle(p, 10);
		System.out.println("c1 == c2 ? " + (c1 == c2));
		
		System.out.println("c1 and c2 are identical? " + areIdentical(c1, c2));
		
		System.out.println("c1 equals c2? " + c1.equals(c2));
	}

}
