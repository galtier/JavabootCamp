package bootcamp.part2._02_object;

public class Circle {
	
	Point center;
	int diameter;
	
	public Circle(Point center, int diameter) {
		this.center = center;
		this.diameter = diameter;
	}

}
